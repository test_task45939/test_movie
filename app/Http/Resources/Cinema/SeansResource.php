<?php

namespace App\Http\Resources\Cinema;

use App\Models\Age;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SeansResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public static $wrap = false;
    public function toArray(Request $request): array
    {
        $age = Age::query()->where('id', $this->age_id)->pluck('age')->first();
        return [
            'title' => $this->title,
            'age' => $age,
        ];
    }
}
