<?php

namespace App\Http\Resources\Cinema;

use App\Http\Resources\Admin\MovieResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class CinemaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'movie' => new SeansResource($this->movie),
            'date' => $this->date->format('d-m H:i'),
            'price' => $this->price->price,

        ];
    }
}
