<?php

namespace App\Http\Resources\Admin;

use App\Models\Age;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $age = Age::where('id', $this->age_id)->pluck('age')->toArray()[0];
        return [
            'id' => $this->id,
            'title' => $this->title,
            'descriptions' => $this->descriptions,
            'image' => $this->image,
            'age' => $age,
        ];
    }
}
