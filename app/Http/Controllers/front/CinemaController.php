<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use App\Http\Resources\Cinema\CinemaResource;
use App\Models\Seans;
use Illuminate\Http\Request;

class CinemaController extends Controller
{
    public function index()
    {
        $seanses = CinemaResource::collection(Seans::query()->latest()->get())->resolve();
        return inertia('cinema/index', compact('seanses'));
    }

}
