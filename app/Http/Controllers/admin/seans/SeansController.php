<?php

namespace App\Http\Controllers\admin\seans;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seans\StoreRequest;
use App\Http\Requests\Seans\UpdateRequest;
use App\Models\Movie;
use App\Models\Price;
use App\Models\Seans;
use Carbon\Carbon;

class SeansController extends Controller
{
    public function index()
    {
        $seanses = Seans::query()->latest()->with('movie')->get();
        return view('admin.seans.index', compact('seanses'));
    }

    public function show(Seans $seans)
    {
        return view('admin.seans.show', compact('seans'));
    }

    public function create()
    {
        $movies = Movie::query()->get();
        if (Seans::query()->latest()->first() !== null) {
            $seans = Seans::query()->latest()->first()->endAt;
        } else {
            $seans = '';
        }
        $prices = Price::all();
        return view('admin.seans.create', compact('movies', 'prices', 'seans'));
    }

    public function edit(Seans $seans)
    {
        $movies = Movie::all();
        $prices = Price::all();
        return view('admin.seans.edit', compact('seans', 'movies', 'prices'));
    }
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        if (Seans::query()->latest()->first() !== null) {
            $lastSeans = Seans::query()->latest()->first();
            $diffMinutes = $lastSeans->endAt->diffInMinutes(Carbon::parse($data['date']), false);
            if ($diffMinutes < 30) {
                return redirect()->back()->with('error', 'Начало сеанса должно быть больше 30 минут');
            }
        }
        Seans::create($data);

        return redirect()->route('admin.seans.index');

    }

    public function update(UpdateRequest $request, Seans $seans)
    {
        $data = $request->validated();
        $seans->update($data);
        return redirect()->route('admin.seans.index');
    }

    public function delete(Seans $seans)
    {
        $seans->delete();
        return redirect()->route('admin.seans.index');
    }
}
