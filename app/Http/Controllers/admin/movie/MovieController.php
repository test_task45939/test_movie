<?php

namespace App\Http\Controllers\admin\movie;

use App\Http\Controllers\Controller;
use App\Http\Requests\Movie\StoreRequest;
use App\Http\Requests\Movie\UpdateRequest;
use App\Models\Age;
use App\Models\Movie;
use App\Models\MoviePicture;
use Illuminate\Support\Facades\Storage;

class MovieController extends Controller
{
    public function index()
    {
        $movies = Movie::query()->latest()->get();
        return view('admin.movie.index', compact('movies'));
    }

    public function show(Movie $movie)
    {
        return view('admin.movie.show', compact('movie'));
    }

    public function edit(Movie $movie)
    {
        $ages = Age::all();
        return view('admin.movie.edit', compact('movie', 'ages'));
    }

    public function create()
    {
        $ages = Age::all();
        return view('admin.movie.create', compact('ages'));
    }

    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $image = $data['image'];
        unset($data['image']);
        $movie = Movie::firstOrCreate($data);
        $path = Storage::disk('public')->putFileAs('/images/movie',$image, $movie->id . '.jpg', $image);
        $url = $request->root() . '/storage/' . $path;
        MoviePicture::create([
            'path' => $path,
            'url' => $url,
            'movie_id' => $movie->id
        ]);
        return redirect()->route('admin.movie.index');
    }

    public function update(UpdateRequest $request, Movie $movie)
    {
        $data = $request->validated();
        if (isset($data['image'])) {
            $image = $data['image'];
            unset($data['image']);
            $path = Storage::disk('public')->putFileAs('/images/movie',$image, $movie->id . '.jpg', $image);
            $url = $request->root() . '/storage/' . $path;
            MoviePicture::query()->update([
                'path' => $path,
                'url' => $url,
                'movie_id' => $movie->id
            ]);

        }

        $movie->update($data);
        return redirect()->route('admin.movie.show', $movie->id);
    }

    public function delete(Movie $movie)
    {
        if (isset($movie->image->path)) {
            Storage::disk('public')->delete($movie->image->path);
        }
        $movie->delete();
        return redirect()->route('admin.movie.index');
    }
}
