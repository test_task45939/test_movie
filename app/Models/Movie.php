<?php

namespace App\Models;

use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Movie extends Model
{
    use HasFactory;
    protected $guarded = false;
    protected $table = 'movies';

    public function image(): HasOne
    {
        return $this->hasOne(MoviePicture::class);
    }

    public function getAgeAttribute(): string
    {
        return Age::where('id', $this->age_id)->pluck('age')->first() . '+';
    }

    public function getTimeAttribute()
    {
        return CarbonInterval::seconds($this->duration)->cascade()->forHumans();
    }


}
