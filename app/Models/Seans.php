<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Seans extends Model
{
    use HasFactory;
    protected $guarded = false;
    protected $table = 'seans';



    public function movie(): BelongsTo
    {
        return $this->belongsTo(Movie::class);
    }

    public function price(): BelongsTo
    {
        return $this->belongsTo(Price::class);
    }

    public function endAt(): Attribute
    {
        return Attribute::make(
            get: fn() => $this->date->addSeconds($this->movie->duration)
        );
    }

    protected $casts = [
        'date' => 'datetime'
    ];
}
