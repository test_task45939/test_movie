@extends('layouts.admin.main')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Добавить сеанс</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Главная</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <form action="{{ route('admin.seans.store') }}" method="post" class="w-96">
                    @csrf
                    <div class="form-group">
                        <label>Выберете возрастное ограничение</label>
                        <select name="movie_id" class="form-control select2" style="width: 100%;">
                            @foreach($movies as $movie)
                                <option value="{{ $movie->id }}"
                                    {{ $movie->id == old('movie_id') ? ' selected' : '' }}>
                                    {{ $movie->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="localdate"> Дата и время:
                            @if ($seans != '')
                            <p>Конец предыдущего сеанса {{ $seans }}</p>
                                <p>Следущий сеанс должен начаться через 30 минут</p>
                            @endif
                        </label>
                        <p><input type="datetime-local" id="localdate" name="date" value="{{ old('date') }}"/></p>
                        {{ session('error') }}
                    </div>
                    <div class="form-group">
                        <label>Цена</label>
                        <select name="price_id" class="form-control select2" style="width: 100%;">
                            @foreach($prices as $price)
                                <option value="{{ $price->id }}" {{ $price->price == old('price') ? ' selected' : '' }}>
                                    {{ $price->price }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" value="Добавить" class="btn btn-success">
                </form>
            </div>
        </div>
    </section>
@endsection

