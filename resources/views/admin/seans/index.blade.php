@extends('layouts.admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Сеансы</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Главная</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header flex">
                            <a href="{{ route('admin.seans.create') }}" class="btn btn-primary">Добавить Сеанс</a>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>Фильм</th>
                                    <th>Время и дата</th>
                                    <th>Цена</th>
                                    <th>Удаление</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($seanses as $seans)
                                    <tr>
                                        <td><a href="{{ route('admin.seans.show', $seans->id) }}">{{ $seans->movie->title }}</a></td>
                                        <td>{{ $seans->date }}</td>
                                        <td>{{ $seans->price->price }}</td>
                                        <td><form action="{{ route('admin.seans.delete', $seans->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="submit" value="Удалить" class="btn btn-danger">
                                            </form></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="flex justify-content-center">
{{--                        {{ $movies->links() }}--}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

<style>

</style>
