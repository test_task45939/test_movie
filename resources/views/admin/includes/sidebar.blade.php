<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-header">Manage</li>
        <li class="nav-item">
            <a href="{{ route('admin.movie.index') }}" class="nav-link">
                <i class="nav-icon far fa-calendar-alt"></i>
                <p>
                    Фильмы
                    <span class="badge badge-info right">{{ \App\Models\Movie::query()->count() }}</span>
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.seans.index') }}" class="nav-link">
                <i class="nav-icon far fa-calendar-alt"></i>
                <p>
                    Сеансы
                    <span class="badge badge-info right">{{ \App\Models\Seans::query()->count() }}</span>
                </p>
            </a>
        </li>
    </ul>
</nav>

