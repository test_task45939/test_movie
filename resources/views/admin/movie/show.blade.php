@extends('layouts.admin.main')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Фильмы</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Главная</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header flex">
                            <a href="{{ route('admin.movie.edit', $movie->id) }}" class="btn btn-primary">Изменить Фильм</a>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Фото</th>
                                    <th>Описание</th>
                                    <th>Продолжительность</th>
                                    <th>Возраст</th>
                                    <th>Удаление</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                        <td>{{ $movie->title }}</td>
                                        <td class="w-25"><img class="w-25" src="{{ $movie->image->url }}" alt="{{ $movie->title }}"></td>
                                        <td>{{ $movie->descriptions }}</td>
                                        <td>{{ $movie->time }}</td>
                                        <td>{{ $movie->age }}</td>
                                        <td><form action="{{ route('admin.movie.delete', $movie->id) }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="submit" value="Удалить" class="btn btn-danger">
                                            </form></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

<style>

</style>
