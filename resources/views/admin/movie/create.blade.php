@extends('layouts.admin.main')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Добавить фильм</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Главная</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <form action="{{ route('admin.movie.store') }}" method="post" enctype="multipart/form-data" class="w-96">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="title" class="form-control" placeholder="название" value="{{ old('title') }}">
                        @error('title')
                        {{ $message }}
                        @enderror
                    </div>
                    <div class="form-group mb-2">
                        <label>Добавить главное фото</label>
                        <div class="custom-file">
                            <input type="file" name="image" multiple class="custom-file-input" id="inputGroupFile01"
                                   aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            @error('image')
                            {{ $message }}
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="descriptions" placeholder="описание">{{ old('descriptions') }}</textarea>
                        @error('descriptions')
                        {{ $message }}
                        @enderror
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="number" name="duration" placeholder="продолжительность" value="{{ old('duration') }}">
                        @error('duration')
                        {{ $message }}
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Выберете возрастное ограничение</label>
                        <select name="age_id" class="form-control select2" style="width: 100%;">
                            @foreach($ages as $age)
                                <option value="{{ $age->id }}"
                                    {{ $age->id == old('age_id') ? ' selected' : '' }}>
                                    {{ $age->age . '+'}}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" value="Добавить" class="btn btn-success">
                </form>
            </div>
        </div>
    </section>
@endsection

