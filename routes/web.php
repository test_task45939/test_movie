<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth', 'isAdmin')->prefix('admin')->group(function () {
    Route::get('/', \App\Http\Controllers\admin\main\IndexController::class);
    Route::prefix('movie')->group(function () {
        Route::get('/', [\App\Http\Controllers\admin\movie\MovieController::class, 'index'])->name('admin.movie.index');
        Route::post('/', [\App\Http\Controllers\admin\movie\MovieController::class, 'store'])->name('admin.movie.store');
        Route::get('/show/{movie}', [\App\Http\Controllers\admin\movie\MovieController::class, 'show'])->name('admin.movie.show');
        Route::get('/create', [\App\Http\Controllers\admin\movie\MovieController::class, 'create'])->name('admin.movie.create');
        Route::get('/edit/{movie}/', [\App\Http\Controllers\admin\movie\MovieController::class, 'edit'])->name('admin.movie.edit');
        Route::patch('/edit/{movie}/', [\App\Http\Controllers\admin\movie\MovieController::class, 'update'])->name('admin.movie.update');
        Route::delete('/delete/{movie}/', [\App\Http\Controllers\admin\movie\MovieController::class, 'delete'])->name('admin.movie.delete');
    });
    Route::prefix('seans')->group(function () {
        Route::get('/', [\App\Http\Controllers\admin\seans\SeansController::class, 'index'])->name('admin.seans.index');
        Route::post('/', [\App\Http\Controllers\admin\seans\SeansController::class, 'store'])->name('admin.seans.store');
        Route::get('/create', [\App\Http\Controllers\admin\seans\SeansController::class, 'create'])->name('admin.seans.create');
        Route::get('/show/{seans}/', [\App\Http\Controllers\admin\seans\SeansController::class, 'show'])->name('admin.seans.show');
        Route::get('/show/{seans}/edit', [\App\Http\Controllers\admin\seans\SeansController::class, 'edit'])->name('admin.seans.edit');
        Route::patch('/show/{seans}/edit', [\App\Http\Controllers\admin\seans\SeansController::class, 'update'])->name('admin.seans.update');
        Route::delete('/delete/{seans}/', [\App\Http\Controllers\admin\seans\SeansController::class, 'delete'])->name('admin.seans.delete');
    });
});

Route::get('/', [\App\Http\Controllers\front\CinemaController::class, 'index'])->name('cinema.index');


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
